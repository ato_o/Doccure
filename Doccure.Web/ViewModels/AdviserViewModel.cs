﻿namespace Doccure.Web.ViewModels
{
    public class AdviserViewModel
    {
        public byte Age { get; set; }
        public string McNo { get; set; }
        public bool Text { get; set; }
        public bool VoiceCall { get; set; }
        public bool VideoCall { get; set; }
        public bool Clinic { get; set; }
        public bool InPlace { get; set; }
        public string Description { get; set; }
        public float Rate { get; set; }
        public int TotalRates { get; set; }
        public int SuccessfulCount { get; set; }
        public int UnsuccessfulCount { get; set; }
        public int CanceledCount { get; set; }
        public int DeclinedCount { get; set; }
        public decimal Fee { get; set; }

    }
}