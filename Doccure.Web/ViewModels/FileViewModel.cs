﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doccure.Web.ViewModels
{
    public class FileViewModel
    {
        public string Path { get; set; }
    }
}
