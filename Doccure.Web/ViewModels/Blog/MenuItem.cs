﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doccure.WEB.ViewModels.Blog
{
    public class MenuItem
    {
        public string Title { get; set; }
        public string Slug { get; set; }

    }
}
