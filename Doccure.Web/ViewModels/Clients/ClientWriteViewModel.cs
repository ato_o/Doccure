﻿using System;
using System.ComponentModel.DataAnnotations;
using Doccure.Core.Types;
using Doccure.Core.Users;
using Doccure.Web.Code;
using Doccure.Web.ViewModels.Users;
using Microsoft.AspNetCore.Http;

namespace Doccure.Web.ViewModels.Clients
{
    public class ClientWriteViewModel
    {
        public UserWriteViewModel User { get; set; } = new UserWriteViewModel();
        public int UserId { get; set; }
        public DateTime Birthday { get; set; }
        public string PersonCaseHistory { get; set; }//the app doesn't take more info about these fields
        public string FamilyCaseHistory { get; set; }
        public string SurgeryHistory { get; set; }
        [UIHint("EnumDropdown")]
        [Display(Name = "Blood group")]
        public BloodGroupType BloodGroupType { get; set; }

        [UIHint("EnumDropdown")]
        [Display(Name = "Gender", Prompt = "Gender")]
        public Gender Gender { get; set; }

    }
}
