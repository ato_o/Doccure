﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Doccure.Core.Types;
using Doccure.Web.Code;
using Microsoft.AspNetCore.Http;

namespace Doccure.Web.ViewModels.Users
{
    public class FileViewModel
    {
        [Display(Name = "picture profile")]
        [DataType(DataType.Upload)]
        [MaxFileSize(10 * 1024 * 1024)] //10mgb
        [AllowedExtensions(new string[] { ".jpg", ".png" })]
        public IFormFile ProfilePic { get; set; }
        public string Path { get; set; }
        public FileType Type { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Size { get; set; }
        public string Duration { get; set; }
    }
}
