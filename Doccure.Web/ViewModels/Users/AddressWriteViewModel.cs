﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Doccure.Web.ViewModels.Users
{
    public class AddressWriteViewModel
    {
        [Display(Name = "State", Prompt = "State")]
        [UIHint("EditorString")]
        [Required(ErrorMessage = "State is required!")]
        public string State { get; set; }
        [Display(Name = "City", Prompt = "City")]
        [UIHint("EditorString")]
        [Required(ErrorMessage = "City is required!")]
        public string City { get; set; }
        [Display(Name = "Part2", Prompt = "Part2")]
        [UIHint("EditorString")]
        public string Part2 { get; set; }
        [Display(Name = "Zip", Prompt = "Zip")]
        [UIHint("EditorString")]
        public string PostalCode { get; set; }
        public float Lat { get; set; }
        public float Lng { get; set; }
    }
}
