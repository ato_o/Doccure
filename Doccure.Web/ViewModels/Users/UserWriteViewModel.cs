﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Doccure.Core.Types;
using Doccure.Web.Code;
using Microsoft.AspNetCore.Http;

namespace Doccure.Web.ViewModels.Users
{
    public class UserWriteViewModel
    {
        [RegularExpression("\\b([a-zA-ZÀ-ÿ][-,a-z. ']+[ ]*)+$", ErrorMessage = "Enter the correct format of your first name")]
        [Required(ErrorMessage = "First name is required!")]
        [Display(Name = "Firstname", Prompt = "Firstname")]
        [UIHint("EditorString")]
        public string FirstName { get; set; }

        [RegularExpression("\\b([a-zA-ZÀ-ÿ][-,a-z. ']+[ ]*)+", ErrorMessage = "Enter the correct format of your last name")]
        [Required(ErrorMessage = "Last name is required")]
        [Display(Name = "Lastname", Prompt = "Lastname")]
        [UIHint("EditorString")]
        public string LastName { get; set; }

        [RegularExpression("^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$", ErrorMessage = "Enter correct format of username.")]
        [Required(ErrorMessage = "Username name is required")]
        [Display(Name = "Username", Prompt = "Username")]
        [UIHint("EditorString")]
        public string UserName { get; set; }

        [RegularExpression("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{5,}$", ErrorMessage = "Password at least contains 4 english letters and a number")]
        [Required(ErrorMessage = "Password name is required")]
        [Display(Name = "Password", Prompt = "Password")]
        [UIHint("EditorString")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Please, repeat password!")]
        [Display(Name = "Confirm password", Prompt = "Repeat password!")]
        [Required(ErrorMessage = "Repeat your password!")]
        [UIHint("EditorString")]
        public string ConfirmPassword { get; set; }

        [RegularExpression("^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}$", ErrorMessage = "Email must be like you@ex.ex")]
        [Required(ErrorMessage = "Email is required!")]
        [Display(Name = "Email", Prompt = "Email")]
        [UIHint("EditorString")]
        public string Email { get; set; }

        public bool IsActive { get; set; }// delete Account
        public bool IsOnline { get; set; }
        public DateTime LastSeen { get; set; }
        public decimal Credit { get; set; }
        public UserRoleType UserRoleType { get; set; }
        [UIHint("EditorString")]
        [Display(Name = "Age", Prompt = "Age")]
        [Required(ErrorMessage = "Age is required!")]
        public string Age { get; set; }
        [RegularExpression("True", ErrorMessage = "Check!")]
        public bool IsAgree { get; set; }

        public int? AppFileId { get; set; }
        public AddressWriteViewModel Address { get; set; }

        public FileViewModel AppFile { get; set; } = new FileViewModel();

    }

}
