﻿
namespace Doccure.Web.ViewModels
{
    public class CategoryViewModel:FileViewModel
    {
        public string Title { get; set; }
        public string Slug { get; set; }

    }
}