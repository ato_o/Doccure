﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Doccure.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Doccure.Web.ViewComponents
{
    public class TopAdvisersViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new List<AdviserViewModel>();
            return View(model);
        }
    }
}
