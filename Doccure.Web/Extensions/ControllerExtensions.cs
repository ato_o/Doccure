﻿using Doccure.Web.Code;
using Microsoft.AspNetCore.Mvc;


namespace Doccure.Web.Extensions
{
    public static class ControllerExtensions
    {
        public static void SetNotification(this Controller controller, DbResult result)
        {
            controller.TempData.Set("db", result);
            controller.ModelState.TakeError(result);
        }

    }
}
