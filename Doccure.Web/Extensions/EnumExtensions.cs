﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Doccure.Web.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescriptionAttributeFrom(this Enum enumValue)
        {
            var description = "";
            var enumType = enumValue.GetType();
            var info = enumType.GetMember(enumValue.ToString()).First();

            if (info != null && info.CustomAttributes.Any())
            {
                var nameAttr = info.GetCustomAttribute<DescriptionAttribute>();
                description = nameAttr != null ? nameAttr.Description : enumValue.ToString();
            }
            else
            {
                description = enumValue.ToString();
            }
            return description;
        }


        public static List<string> ToList(this Enum enumValue)
        {
            return
                Enum.GetValues(enumValue.GetType())
                    .Cast<Enum>()
                    .Select(r => r.GetDescriptionAttributeFrom())
                    .ToList();
        }

        //radio button generating
        public static List<SelectListItem> ToSelectListItems(this Enum enumValue, Enum selectedValue)
        {
            if (enumValue == null) return new List<SelectListItem>();
            return
                Enum.GetValues(enumValue.GetType())
                    .Cast<Enum>()
                    .Select(r => new SelectListItem()
                    {
                        Text = r.GetDescriptionAttributeFrom(),
                        Value = r.ToString(),
                        Selected = r.Equals(selectedValue)
                    }).ToList();
        }
        //dropdown generating
        public static List<SelectListItem> ToSelectListItems(this Enum enumValue)
        {
            if (enumValue == null) return new List<SelectListItem>();
            return
                Enum.GetValues(enumValue.GetType())
                    .Cast<Enum>()
                    .Select(r => new SelectListItem()
                    {
                        Text = r.GetDescriptionAttributeFrom(),
                        Value = r.ToString(),
                    }).ToList();
        }
    }


}
