﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Doccure.Web.Extensions
{
    public static class FileExtensions
    {
        public static IDictionary<string, string> GetPictureProperties(this IFormFile pic)
        {
            var dictionary = new Dictionary<string, string>();
            dictionary["Size"] = pic.Length.ToString();
            //dictionary["Height"] = pic.
            //dictionary["Width"]
            return dictionary;
        }
    }
}
