﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Doccure.Web.Code;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Doccure.Web.Extensions
{
    public static class ModelStateDictionaryExtensions
    {
        public static bool TakeError(this ModelStateDictionary modelState, DbResult result)
        {
            foreach (var (key, value) in result.Errors)
            {
                modelState.AddModelError(key, value);
            }

            if (!string.IsNullOrEmpty(result.Message))
            {
                modelState.AddModelError("", result.Message);
            }

            if (result.Done)
            {
                return true;
            }

            return false;
        }
    }
}
