﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;

namespace Doccure.Web.Extensions
{
    public static class TempDataExtensions
    {
        public static void Set(this ITempDataDictionary tempData, string key, object data)
        {
            tempData[key] = JsonConvert.SerializeObject(data);
        }

        public static T Get<T>(this ITempDataDictionary tempData, string key)
        {
            return JsonConvert.DeserializeObject<T>(tempData[key].ToString());
        }
    }
}
