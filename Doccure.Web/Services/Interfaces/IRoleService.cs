﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Doccure.Web.Code;
using Doccure.Web.ViewModels;

namespace Doccure.Web.Services.Interfaces
{
    public interface IRoleService
    {
        Task<DbResult> CreateRole(RoleItem roleItem);
        Task<DbResult> DeleteRole();
        RoleItem ReadRole(in int id);
    }
}
