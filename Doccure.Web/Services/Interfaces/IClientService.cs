﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Doccure.Web.Code;
using Doccure.Web.ViewModels.Clients;

namespace Doccure.Web.Services.Interfaces
{
    public interface IClientService
    {
        Task<DbResult> CreateClient(ClientWriteViewModel model);
    }
}
