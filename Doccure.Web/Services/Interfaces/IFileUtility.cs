﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Doccure.Core;
using Doccure.Web.Code;
using Doccure.Web.ViewModels.Users;
using Microsoft.AspNetCore.Http;

namespace Doccure.Web.Services.Interfaces
{
    public interface IFileUtility
    {
        FileViewModel SavePicAsync(IFormFile pic, string dir);

        string SaveVideoAsync(IFormFile video, string dir);

        string SaveAudioAsync(IFormFile audio, string dir);

        string SaveDocAsync(IFormFile doc, string dir);
    }
}
