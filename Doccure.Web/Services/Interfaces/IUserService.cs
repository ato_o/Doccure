﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Doccure.Core.Types;
using Doccure.Web.Code;
using Doccure.Web.ViewModels.Users;

namespace Doccure.Web.Services.Interfaces
{
    public interface IUserService
    {
        Task<int> CreateUser(UserWriteViewModel model, DbResult dbResult,UserRoleType roleType);
    }
}
