﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Doccure.Web.Code;
using Doccure.ViewModels;
using Doccure.Web.ViewModels;

namespace Doccure.Web.Services.Interfaces
{
    public interface IHomeService
    {
        List<AdviserViewModel> GetAdviserByMaxRate();
        List<CategoryViewModel> GetCategory();
        List<AdviserViewModel> SearchAdvisers();

    }
}
