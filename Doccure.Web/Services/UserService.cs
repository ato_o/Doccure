﻿using System.Linq;
using System.Threading.Tasks;
//using AutoMapper;
using Doccure.Core;
using Doccure.Core.Types;
using Doccure.Core.Users;
using Doccure.Web.Code;
using Doccure.Web.Services.Interfaces;
using Doccure.Web.ViewModels.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Doccure.Web.Services
{
    public class UserService : IUserService
    {
        //private readonly IMapper _mapper;

        private readonly DbAssitant _db;
        private readonly IFileUtility _fileUtility;
        public UserService(/*IMapper mapper,*/  DbAssitant db, IFileUtility fileUtility)
        {
            //_mapper = mapper;
            _db = db;
            _fileUtility = fileUtility;
        }
        public async Task<int> CreateUser(UserWriteViewModel model, DbResult dbResult, UserRoleType roleType)
        {
            if (model.AppFile.ProfilePic == null)
            {
                model.AppFileId = null;
            }
            //create user
            else
            {
                model.AppFileId = CreateFile(model.AppFile.ProfilePic, "pictures");
            }

            //var user = _mapper.Map<User>(model);
            //if (_db.Users.Any(x => x.NormalizedUserName == model.UserName.ToUpper()))
            //{
            //    dbResult.ResultType = ResultType.Error;
            //    dbResult.Errors.Add("UserName", "This user name is already taken!");
            //    return 0;
            //}
            //if (_db.Users.Any(x => x.NormalizedEmail == model.Email.ToUpper()))
            //{
            //    dbResult.ResultType = ResultType.Error;
            //    dbResult.Errors.Add("Email", "This email is already taken!");
            //    return 0;
            //}

            //var resultUser = await _userManager.CreateAsync(user, model.Password);
            //if (_db.Save().Done)
            //{
            //    var userId = user.Id;
            //    return userId;
            //}
            //add registered user role
            //var user = await _userManager.FindByEmailAsync("atefe.Rajabi.78@gmail.com");
            //if (user != null)
            //{
            //    var result = await _userManager.AddToRoleAsync(user, "Admin");
            //    return Content(result.Succeeded.ToString());
            //}
            return 0;
        }
        public int CreateFile(IFormFile pic, string dir)
        {
            var fileViewModel = _fileUtility.SavePicAsync(pic, dir);
            var file = _mapper.Map<AppFile>(fileViewModel);
            _db.AppFiles.Add(file);
            int fileId = 0;
            if (_db.Save().Done)
            {
                fileId = file.Id;
            }
            return fileId;
        }
    }
}
