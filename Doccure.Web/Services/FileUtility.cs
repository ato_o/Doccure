﻿using System;
using System.IO;
using Doccure.Web.Extensions;
using Doccure.Web.Services.Interfaces;
using Doccure.Web.ViewModels.Users;
using Microsoft.AspNetCore.Http;

namespace Doccure.Web.Services
{
    public class FileUtility : IFileUtility
    {
        public FileViewModel SavePicAsync(IFormFile pic, string dir)
        {
            var file = new FileViewModel();
            if (pic != null && pic.Length > 0)
            {
                var name = Path.GetRandomFileName().Split(".")[0] + Path.GetExtension(pic.FileName);
                try
                {
                    using (var stream = File.Create("wwwroot/" + dir + "/" + name))
                    {
                        pic.CopyTo(stream);
                    }

                    file.Path = dir + "/" + name;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            file.Size = pic.GetPictureProperties()["Size"];
            file.Duration = "0";
            return file;
        }

        public string SaveVideoAsync(IFormFile video, string dir)
        {
            var videoPath = "";
            if (video != null && video.Length > 0)
            {

                var name = Path.GetRandomFileName().Split(".")[0] + Path.GetExtension(video.FileName);
                try
                {
                    using (var stream = File.Create("wwwroot/" + dir + "/" + name))
                    {
                        video.CopyTo(stream);
                    }

                    videoPath = dir + "/" + name;
                }
                catch (Exception e)
                {

                }
            }

            return videoPath;
        }

        public string SaveAudioAsync(IFormFile audio, string dir)
        {
            var audioPath = "";
            if (audio != null && audio.Length > 0)
            {
                var name = Path.GetRandomFileName().Split(".")[0] + Path.GetExtension(audio.FileName);
                try
                {
                    using (var stream = File.Create("wwwroot/" + dir + "/" + name))
                    {
                        audio.CopyTo(stream);
                    }
                    audioPath = dir + "/" + name;
                }
                catch (Exception e)
                {
                    Console.WriteLine("---------------------------------------------" + "doc errrrrrorrrrr");
                }
            }
            return audioPath;
        }

        public string SaveDocAsync(IFormFile doc, string dir)
        {
            var docPath = "";
            if (doc != null && doc.Length > 0)
            {

                var name = Path.GetRandomFileName().Split(".")[0] + Path.GetExtension(doc.FileName);
                try
                {
                    using (var stream = File.Create("wwwroot/" + dir + "/" + name))
                    {
                        doc.CopyTo(stream);
                    }

                    docPath = dir + "/" + name;
                }
                catch (Exception e)
                {
                    Console.WriteLine("---------------------------------------------" + "doc errrrrrorrrrr");
                }

            }

            return docPath;
        }
        
    }
}
