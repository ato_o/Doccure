﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Doccure.Core;
using Doccure.Core.Clients;
using Doccure.Core.Types;
using Doccure.Core.Users;
using Doccure.Web.Code;
using Doccure.Web.Services.Interfaces;
using Doccure.Web.ViewModels.Clients;
using Doccure.Web.ViewModels.Users;
using Microsoft.AspNetCore.Identity;

namespace Doccure.Web.Services
{
    public class ClientService : IClientService
    {
        private readonly DbAssitant _db;
        //private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IFileUtility _fileUtility;

        public ClientService(DbAssitant db/*, IMapper mapper*/, IFileUtility fileUtility, IUserService userService)
        {
            _db = db;
            //_mapper = mapper;
            _fileUtility = fileUtility;
            _userService = userService;
        }

        public async Task<DbResult> CreateClient(ClientWriteViewModel model)
        {
            var dbResult = new DbResult();
            //var user = _mapper.Map<UserWriteViewModel>(model);
            var query = $"";

            return dbResult;
        }
    }
}
