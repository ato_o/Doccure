﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Doccure.Web.Code
{
    public class DbAssitant
    {

        private readonly string _connectionString = "Server = ATENA\\ATEFE; Database = DoccureDb; Trusted_Connection = True; MultipleActiveResultSets = true";
        private SqlCommand _command;
        private SqlConnection _connection;


        public DbAssitant()
        {
            try
            {
                _connection = new SqlConnection(_connectionString);
                _connection.Open();
            }

            catch (Exception e)
            {
                var result = new DbResult();
                result.Errors.Add("db", e.Message);
            }
        }


        public void OpenConnection()
        {
            try
            {

                _connection = new SqlConnection(_connectionString);
                _connection.Open();
            }

            catch (Exception e)
            {
                var result = new DbResult();
                result.Errors.Add("db", e.Message);
            }
        }

        public void CloseConnection()
        {
            _connection.Close();
        }

        //Insert
        public int InsertData(String query)
        {
            int result;
            _command = new SqlCommand(query, _connection);
            result = _command.ExecuteNonQuery();
            return result;
        }

        public SqlDataReader GetData(String query)
        {
            _command = new SqlCommand(query, _connection);
            SqlDataReader reader = _command.ExecuteReader();
            return reader;
        }


        public int UpdateData(String query)
        {
            int result;
            _command = new SqlCommand(query, _connection);
            result = _command.ExecuteNonQuery();
            return result;

        }

        public int DeleteData(String query)
        {
            int result;
            _command = new SqlCommand(query, _connection);
            result = _command.ExecuteNonQuery();
            return result;

        }
    }
}
