﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doccure.Web.Code
{
    public class DbResult
    {
        public bool Done { get; set; }
        public IDictionary<string, string> Errors { get; set; }
        public string Message { get; set; }

        public DbResult()
        {
            Errors = new Dictionary<string, string>();
        }
    }
}
