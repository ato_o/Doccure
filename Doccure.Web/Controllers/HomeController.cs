﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Doccure.ViewModels;
using Doccure.Web.Services.Interfaces;

namespace Doccure.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHomeService _homeService;
        public HomeController(IHomeService homeService)
        {
            _homeService = homeService;
        }
        public IActionResult Index()
        {
            //feed index
            var data = _homeService.GetAdviserByMaxRate();
            return View();
        }
    }

}
