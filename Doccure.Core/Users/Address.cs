﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Doccure.Core.Users
{
    public class Address
    {
        public virtual AppUser AppUser { get; set; }
        public int AppUserId { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Part2 { get; set; }
        public string PostalCode { get; set; }
        public float Lat { get; set; }
        public float Lng { get; set; }

    }
}
