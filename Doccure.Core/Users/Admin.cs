﻿using System.Collections.Generic;
using Doccure.Core.Types;
using Doccure.Core;
using Doccure.Core.Blog;
using Doccure.Core.Reports;

namespace Doccure.Core.Users
{
    public class Admin : BaseEntity
    {
        public virtual AppUser AppUser { get; set; }
        public int AppUserId { get; set; }

        public string NationalCode { get; set; }

        public AdminRoleType RoleType { get; set; }
        //1-N Relations
        public virtual List<Post> Posts { get; set; }
        public virtual List<Report> CheckReports { get; set; }

    }
}