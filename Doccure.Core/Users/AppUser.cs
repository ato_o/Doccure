﻿using System;
using System.Collections.Generic;
using Doccure.Core.Reports;
using Doccure.Core.Blog;
using Doccure.Core.Doctor;
using Doccure.Core.Financial;
using Doccure.Core.Types;

namespace Doccure.Core.Users
{
    public class AppUser : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }// delete Account
        public bool IsOnline { get; set; }
        public DateTime LastSeen { get; set; }
        public decimal Credit { get; set; }
        public UserRoleType UserRoleType { get; set; }

        // 1-N Relations
        public virtual List<MobileNumber> MobileNumbers { get; set; }
        public virtual List<PhoneNumber> PhoneNumbers { get; set; }
        public virtual List<Report> Reporters { get; set; }
        public virtual List<Comment> Comments { get; set; }

        public virtual List<Report> Reporteds { get; set; }
        public virtual List<OrderFiTransaction> OrderFiTransactions { get; set; }

        public virtual List<FiTransaction> FiTransactions { get; set; }


    }
}
