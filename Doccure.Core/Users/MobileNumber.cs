﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Doccure.Core.Users
{
    public class MobileNumber
    {
        public virtual AppUser AppUser { get; set; }
        public int AppUserId { get; set; }
        public string Number { get; set; }
    }
}
