﻿using System;
using System.Collections.Generic;
using Doccure.Core.Doctor;
using Doccure.Core.Financial;
using Doccure.Core.Types;
using Doccure.Core.Users;

namespace Doccure.Core.Clients
{
    public class Client : BaseEntity
    {
        public DateTime Birthday { get; set; }
        public string PersonCaseHistory { get; set; }//the app doesn't take more info about these fields
        public string FamilyCaseHistory { get; set; }
        public string SurgeryHistory { get; set; }
        public BloodGroupType BloodGroupType { get; set; }

        //N - M
        public virtual List<Follow> Follows { get; set; }
        public virtual List<BuyKit> BuyKits { get; set; }
        public virtual List<OrderFiTransaction> OrderFiTransactions { get; set; }

        // 1 - 1
        public AppUser AppUser { get; set; }
        public int AppUserId { get; set; }

    }
}
