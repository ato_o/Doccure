﻿namespace Doccure.Core.Types
{
    public enum FileKitType : byte
    {
        Picture = 1,
        Document = 2,
    }
}