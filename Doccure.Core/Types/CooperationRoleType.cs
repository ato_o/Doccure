﻿using System.ComponentModel;

namespace Doccure.Core.Types
{
   public enum CooperationRoleType:byte
    {
        [Description("روانشناس")]
        Psychologist=1,
        [Description("دامپزشک")]
        Vet =2,
        [Description("بهیار")]
        Paramedic = 3,
        [Description("ارتوپد")]
        PhysicianAssistant = 4,
        [Description("پرستار")]
        Nurse = 5
    }
}
