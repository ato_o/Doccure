﻿namespace Doccure.Core.Types
{
    public enum MenuPosition:byte
    {
        Top = 1,
        Bottom = 2,
        Left = 3,
        Right = 4,
    }
}
