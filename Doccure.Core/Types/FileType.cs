﻿namespace Doccure.Core.Types
{
    public enum FileType : byte
    {
        Picture = 1,
        Movie = 2,
        Document = 3,
        Kit = 4,
    }
}