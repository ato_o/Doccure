﻿using System.ComponentModel;

namespace Doccure.Core.Types
{
    public enum UserRoleType:byte
    {
        [Description("بیمار")]
        Patient =1,
        [Description("پزشک")]
        Dr =2,
        [Description("مدیریت")]
        Admin =3,
    }
}
