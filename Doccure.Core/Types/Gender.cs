﻿using System.ComponentModel;

namespace Doccure.Core.Types
{
    public enum Gender : byte
    {
        [Description("هیچکدام")]
        Null =0,
        [Description("خانم")]
        Female = 1,
        [Description("آقا")]
        Man = 2,
    }
}
