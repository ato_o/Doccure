﻿using System.ComponentModel;

namespace Doccure.Core.Types
{
    public enum PostType:byte
    {
        [Description("عکس")]
        Pic =1,
        [Description("ویدیو")]
        Video =2,
    }
}
