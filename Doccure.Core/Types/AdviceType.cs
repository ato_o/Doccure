﻿using System.ComponentModel;

namespace Doccure.Core.Types
{
    public enum AdviceType:byte
    {
        [Description("تلفنی")]
        Telephone=1,
        [Description("تماس تصویری")]
        Video=2,
        [Description("متنی")]
        Text=3,
        [Description("صوتی")]
        Audio=4,
        [Description("خدمات در محل")]
        InPlace=5,
    }
}
