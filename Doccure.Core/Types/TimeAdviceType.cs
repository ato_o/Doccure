﻿using System.ComponentModel;

namespace Doccure.Core.Types
{
   public enum TimeAdviceType:byte
    {
        [Description("پایه")]
        Basic =1,
        [Description("تکمیلی")]
        Supplementary =2,
        [Description("طولانی")]
        LongTime =3
    }
}
