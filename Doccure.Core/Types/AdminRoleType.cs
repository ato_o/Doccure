﻿namespace Doccure.Core.Types
{
    public enum AdminRoleType : byte
    {
        WebSite = 1,
        ReportChecker = 2,
        PostSetter = 3
    }
}