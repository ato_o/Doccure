﻿using System.ComponentModel;

namespace Doccure.Core.Types
{
    public enum StatusType:byte
    {
        [Description("پذیرش")]
        Accept =1,
        [Description("عدم پذیرش")]
        Reject =2,
        [Description("در حال انتظار")]
        Waiting =3,
    }
}
