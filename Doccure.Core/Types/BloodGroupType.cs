﻿using System.ComponentModel;

namespace Doccure.Core.Types
{
    public enum BloodGroupType
    {
        [Description("A+")]
        A_Pos = 1,
        [Description("A-")]
        A_Neg = 2,
        [Description("B+")]
        B_Pos = 3,
        [Description("B-")]
        B_Neg = 4,
        [Description("AB+")]
        AB_Pos = 5,
        [Description("AB-")]
        AB_Neg = 6,
        [Description("O+")]
        O_Pos = 7,
        [Description("O-")]
        O_Neg = 8
    }
}