﻿namespace Doccure.Core.Types
{
    public enum ReportType : byte
    {
        Comment = 1,
        User = 2
    }
}