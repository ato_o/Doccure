﻿using System.ComponentModel;

namespace Doccure.Core.Types
{
   public enum PayType:byte
    {
        [Description("آنلاین")]
        Online =1,
        [Description("کارت به کارت")]
        Card =2,
        [Description("اعتبار")]
        Account =3
    }
}
