﻿using System.ComponentModel;

namespace Doccure.Core.Types
{
    public enum Week:byte
    {
        
        [Description("شنبه")]
        Saturday =0,
        [Description("یکشنبه")]
        Sunday = 1,
        [Description("دوشنبه")]
        Monday = 2,
        [Description("سه شنبه")]
        Tuesday = 3,
        [Description("چهار شنبه")]
        Wednesday = 4,
        [Description("پنج شنبه")]
        Thursday = 5,
        [Description("جمعه")]
        Friday = 6,


    }
}
