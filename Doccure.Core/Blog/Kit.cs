﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Doccure.Core.Financial;

namespace Doccure.Core.Blog
{
    public class Kit : BaseEntity
    {
        public decimal Price { get; set; }
        public string Title { get; set; }
        public int BuyCount { get; set; }
        public int LikeCount { get; set; }
        public string Slug { get; set; }
        public DateTime PublishedTime { get; set; }

        // N - M
        public virtual List<FileKit> FileKits { get; set; }
        //1-N Relations
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public virtual List<Comment> Comments { get; set; }
        //N - M - K Relations
        public virtual List<BuyKit> BuyKits { get; set; }


    }
}
