﻿namespace Doccure.Core.Blog
{
    public class SlideShow:BaseEntity
    {
        public string Title { get; set; }
        public string Pic { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public bool IsActive { get; set; }
    }
}
