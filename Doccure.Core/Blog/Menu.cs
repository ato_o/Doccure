﻿using Doccure.Core.Types;

namespace Doccure.Core.Blog
{
    public class Menu:BaseEntity
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public int DisplayOrder { get; set; }
        public int Parent { get; set; }
        public string Slug { get; set; }
        public MenuPosition Position { get; set; }
    }
}
