﻿using System.Collections.Generic;

namespace Doccure.Core.Blog
{
    public class Tag : BaseEntity
    {
        public string Name { get; set; }
        public string Slug { get; set; }
        public virtual List<PostTag> PostTags { get; set; }
    }
}
