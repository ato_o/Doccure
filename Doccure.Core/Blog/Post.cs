﻿using System;
using System.Collections.Generic;
 
using Doccure.Core.Types;
using Doccure.Core.Users;

namespace Doccure.Core.Blog
{
    public class Post : BaseEntity
    {
        public DateTime CreateTime { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime PublishedTime { get; set; }
        public bool IsShow { get; set; }
        public string Title { get; set; }
        public string MetaDescription { get; set; }
        public string FullDescription { get; set; }
        public string Link { get; set; }
        public int VisitCount { get; set; }

        //N-M Relations
        public virtual List<PostTag> PostTags { get; set; }
        public virtual List<FilePost> FilePosts { get; set; }

        //1-N Relations
        public virtual Admin Admin { get; set; }
        public int AdminId { get; set; }
        public virtual List<Comment> Comments { get; set; }
        public virtual Category Category { get; set; }
        public int CategoryId { get; set; }
    }
}
