﻿using System.Collections.Generic;

namespace Doccure.Core.Blog
{
    public class Category : BaseEntity
    {
        public string Title { get; set; }
        public string Slug { get; set; }
        //1 - 1
        public virtual AppFile AppFile { get; set; }
        public int AppFileId { get; set; }
        //1 - N
        public virtual List<Post> Posts { get; set; }
        public virtual List<Kit> Kits { get; set; }


    }
}
