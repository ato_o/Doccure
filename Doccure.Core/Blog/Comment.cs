﻿using System;
using System.Collections.Generic;
using Doccure.Core;
using Doccure.Core.Blog;
using Doccure.Core.Doctor;
using Doccure.Core.Reports;
 
using Doccure.Core.Users;

namespace Doccure.Core.Blog
{
    public class Comment : BaseEntity
    {
        public string Ip { get; set; }
        public string Context { get; set; }
        public DateTime SentTime { get; set; }
        public DateTime PublishedTime { get; set; }
        public bool IsActive { get; set; }
        public int Parent { get; set; }
        //1 - N
        public virtual AppUser AppUser { get; set; }
        public virtual int AppUserId { get; set; }
        public virtual Kit Kit { get; set; }
        public int? KitId { get; set; }
        public int? PosId { get; set; }
        public virtual Post Post { get; set; }
        public virtual Adviser Adviser { get; set; }
        public int? AdviserId { get; set; }
        public virtual List<Report> Reports { get; set; }


    }
}
