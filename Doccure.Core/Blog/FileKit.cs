﻿using System;
using System.Collections.Generic;
using System.Text;
using Doccure.Core.Types;

namespace Doccure.Core.Blog
{
    public class FileKit
    {
        public int AppFileId { get; set; }
        public virtual AppFile AppFile { get; set; }
        public virtual Kit Kit { get; set; }
        public int KitId { get; set; }
        public FileKitType FileKitType { get; set; }
    }
}
