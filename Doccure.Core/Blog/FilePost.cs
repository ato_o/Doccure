﻿namespace Doccure.Core.Blog
{
    public class FilePost
    {
        public int AppFileId { get; set; }
        public virtual AppFile AppFile { get; set; }
        public virtual Post Post { get; set; }
        public int PostId { get; set; }
    }
}