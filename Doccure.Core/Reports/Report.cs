﻿using System;
using Doccure.Core.Blog;
using Doccure.Core.Types;
using Doccure.Core.Users;
using Doccure.Core.Users;

namespace Doccure.Core.Reports
{
    public class Report : BaseEntity
    {

        public ReportType Type { get; set; }
        public string Description { get; set; }
        public bool IsChecked { get; set; }
        public DateTime ReportTime { get; set; }
        // 1- N
        public virtual Comment Comment { get; set; }
        public int? CommentId { get; set; }
        public virtual AppUser ReportedUser { get; set; }
        public int? ReportedUserId { get; set; }
        public virtual AppUser ReporterUser { get; set; }
        public int ReporterUserId { get; set; }

        public virtual Admin Admin { get; set; }
        public int AdminId { get; set; }
    }
}
