﻿using System;
using System.Collections.Generic;
using System.Text;
using Doccure.Core.Blog;
using Doccure.Core.Clients;
namespace Doccure.Core.Financial
{
    public class BuyKit:BaseEntity
    {
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }

        public virtual FiTransaction FiTransaction { get; set; }
        public int FiTransactionId { get; set; }

        public virtual Kit Kit { get; set; }
        public int KitId { get; set; }
    }
}
