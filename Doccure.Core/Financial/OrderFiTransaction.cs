﻿using System;
using Doccure.Core.Doctor;
using Doccure.Core.Clients;
using Doccure.Core.Types;

namespace Doccure.Core.Financial
{
    public class OrderFiTransaction : BaseEntity
    {
        public virtual Adviser Adviser { get; set; }
        public int AdviserId { get; set; }
        public virtual Client Client { get; set; }
        public int ClientId { get; set; }
        public virtual FiTransaction FiTransaction { get; set; }
        public int FiTransactionId { get; set; }
        public DateTime ReserveTime { get; set; }
        public bool IsPrivate { get; set; }
        public DateTime OrderTime { get; set; }
        public AdviceType AdviceType { get; set; }
        public decimal Price { get; set; }

    }
}