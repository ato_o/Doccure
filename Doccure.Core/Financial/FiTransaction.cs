﻿using System;
using System.Collections.Generic;
using Doccure.Core.Users;

namespace Doccure.Core.Financial
{
    public class FiTransaction:BaseEntity
    {
        public int AppUserId { get; set; }
        public virtual AppUser User { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public DateTime Time { get; set; }
        public bool IsComplete { get; set; }
        public string Note { get; set; }
        public string TraceCode { get; set; }
        public string AuthorityCode { get; set; }

        //N M K
        public virtual List<BuyKit> BuyKits { get; set; }

    }
}
