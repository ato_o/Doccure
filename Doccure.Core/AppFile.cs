﻿using System.Collections.Generic;
using Doccure.Core.Blog;
using Doccure.Core.Types;

namespace Doccure.Core
{
    public class AppFile : BaseEntity
    {
        public string FilePath { get; set; }
        public FileType Type { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Size { get; set; }
        public string Duration { get; set; }
        // 1- N
        public virtual Category Category { get; set; }
        //N - M
        public virtual List<FileKit> FileKits { get; set; }
        public virtual List<FilePost> FilePosts { get; set; }
    }
}