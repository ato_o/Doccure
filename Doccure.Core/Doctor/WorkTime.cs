﻿using System.Collections;
using Doccure.Core.Types;

namespace Doccure.Core.Doctor
{
    public class WorkTime
    {
        public virtual Adviser Adviser { get; set; }
        public int AdviserId { get; set; }
        public bool ServiceType { get; set; }
        public bool TimeType { get; set; }
        public string Saturday { get; set; }
        public string Sunday { get; set; }
        public string Monday { get; set; }
        public string Tuesday { get; set; }
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
    }
}
