﻿using System.Collections.Generic;

namespace Doccure.Core.Doctor
{
    public class Skill:BaseEntity
    {
        public string Education { get; set; }
        public string Speciality { get; set; }
        public virtual Adviser Adviser { get; set; }
        public int AdviserId { get; set; }
    }
}
