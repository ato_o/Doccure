﻿using System.Collections.Generic;
using Doccure.Core.Blog;
using Doccure.Core.Financial;
using Doccure.Core.Reports;
 
using Doccure.Core.Users;

namespace Doccure.Core.Doctor
{
    public class Adviser : BaseEntity
    {

        public byte Age { get; set; }
        public string McNo { get; set; }
        public bool Text { get; set; }
        public bool VoiceCall { get; set; }
        public bool VideoCall { get; set; }
        public bool Clinic { get; set; }
        public bool InPlace { get; set; }
        public string Description { get; set; }
        public float Rate { get; set; }
        public int TotalRates { get; set; }
        public int SuccessfulCount { get; set; }
        public int UnsuccessfulCount { get; set; }
        public int CanceledCount { get; set; }
        public int DeclinedCount { get; set; }
        public decimal Fee { get; set; }
        public int FollowersCount { get; set; }

        //1-N
        public virtual List<Comment> Comments { get; set; }
        public virtual List<OrderFiTransaction> Orders { get; set; }
        public virtual List<Follow> Follows { get; set; }
        public virtual List<Skill> Skills { get; set; }
        public virtual List<WorkTime> WorkTimes { get; set; }


        //1 - 1
        public AppUser User { get; set; }
        public int UserId { get; set; }
    }
}
