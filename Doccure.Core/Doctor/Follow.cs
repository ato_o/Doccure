﻿using Doccure.Core.Clients;

namespace Doccure.Core.Doctor
{
    public class Follow
    {
        public virtual Adviser Adviser { get; set; }
        public int AdviserId { get; set; }
        public virtual Client Client { get; set; }
        public int ClientId { get; set; }

    }
}
